Changed the junit version in the file pom.xml right after downloading the project:

![](image_2022-11-21_23-35-58.png)

This is how junit libraries look like after changing the version in .m2:

![](image_2022-11-21_23-33-40.png)
